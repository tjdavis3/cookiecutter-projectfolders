{{ cookiecutter.project_name }}
================================

{{ cookiecutter.project_short_description }}

The folder contains the following::

  .
  ├── README       
  ├── diagrams .......... Planning diagrams
  ├── documents ......... Supporting documents created
  ├── finance
  │   ├── Orders ........ Placed Orders / Invoices
  │   ├── POs ........... Purchase Requests / Orders
  │   └── budget ........ Budgeting Information
  ├── images ............ Images / Logos, etc.
  ├── meetings .......... Meeting Notes / Agendas
  ├── reference ......... Supporting refernce materials, manuals, etc
  └── requirements ...... Project Requirements
  
