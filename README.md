Project Folder Template
=======================

Creates a standard project folder layout with directories for budgeting, notes, documents, etc.

Requirements
------------
Install `cookiecutter` command line: `pip install cookiecutter`    

Usage
-----
Generate a new Cookiecutter template layout: `cookiecutter gh:tjdavis3@gmail.com/cookiecutter-projectfolders`    

License
-------
This project is licensed under the terms of the [MIT License](/LICENSE)
